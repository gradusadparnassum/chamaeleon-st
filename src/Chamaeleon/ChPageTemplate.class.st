Class {
	#name : #ChPageTemplate,
	#superclass : #Object,
	#category : #Chamaeleon
}

{ #category : #'instance creation' }
ChPageTemplate class >> fromString: aString [
	^ self new
]

{ #category : #defaults }
ChPageTemplate >> defaultEncoding [
	^ 'utf-8'
]

{ #category : #accessing }
ChPageTemplate >> render: aDictionaryOfTemplateVariables [
	self notYetImplemented
]
